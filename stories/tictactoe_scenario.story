Feature: Play tictactoe game and win
Narrative: As a game member, I want to play TicTacToe, So that I can win
Scenario: player who connects horizontal line first wins
Given user browses to http://localhost:8080/ticTacToe/
When first move X is (1,1)
When second move O is (2,1)
When third move X is (1,2)
When fourth move O is (2,2)
When fifth move X is (1,3)
Then winner is X