Learning session according to:<br>
https://www.udemy.com/what-a-java-software-developer-must-know-about-testing/<br>
Small testing excersises using:<br>
TDD,BDD,Junit,TestNG,Hamcrest,AssertJ,Mockito,Jmockit,Selenide,Selenium,Cucumber,Jbehave<br>
Including unit, E2E, stress, smoke and UI tests