package com.sybetech.business;

import java.util.HashSet;

public class TicTacToeGame {
    private static final char UNOCCUPIED = '\0';

    public static final String RESULT_IN_PROGRESS = "In progress";
    static final String RESULT_DRAW = "Draw";
    public static final String RESULT_WINNER = "Winner is %s";

    public static final String COORDINATE_ERR_MSG = "Coordinate must be between 1 and 3";
    public static final String FIELD_OCCUPIED_ERR_MSG = "Field is already occupied!";
    public static final String SAVE_STATE_ERR_MSG = "Could not save state to DB!";
    public static final String DROP_DB_ERR_MSG = "Could not drop DB collection!";

    private Character[][] board = {
        {UNOCCUPIED, UNOCCUPIED, UNOCCUPIED}
        , {UNOCCUPIED, UNOCCUPIED, UNOCCUPIED}
        , {UNOCCUPIED, UNOCCUPIED, UNOCCUPIED}
    };

    private char lastPlayer = UNOCCUPIED;

    private int move = 0;
    private static final int SIZE = 3;

    private TicTacToeGameState state;
    private TicTacToeGameMove gameMove;

    public TicTacToeGame() {
        if (state != null) {
            if (state.clear()) {
                this.state.clear();
                this.state = new TicTacToeGameState();
            } else {
                throw new RuntimeException(DROP_DB_ERR_MSG);
            }
        } else {
            this.state = new TicTacToeGameState();
        }
    }

    public TicTacToeGame(TicTacToeGameState state) {
        if (state.clear()) {
            this.state = state;
        } else {
            throw new RuntimeException(DROP_DB_ERR_MSG);
        }
    }

    public TicTacToeGameMove getGameMove() {
        return gameMove;
    }

    public int getMove() {
        return move;
    }

    public TicTacToeGameState getState() {
        return state;
    }
    //

    /**
     *
        1     2     3
     |-----|-----|-----|-->x
    1|(1,1)|(2,1)|(3,1)|
    2|(1,2)|(2,2)|(3,2)|
    3|(1,3)|(2,3)|(3,3)|
     |-----|-----|-----|
     |
     y
     *
     * Hint: you may use the players ascii values: 'X'=88*3 = 264 => X wins, 'O'=79*3 = 237 => O wins
     **/

    public Character getNextPlayer(){
        return lastPlayer == 'X' ? 'O' : 'X';
    }

    public char getLastPlayer() {
        return lastPlayer;
    }

    public String play(int x, int y) {
        gameMove = new TicTacToeGameMove(++this.move,getNextPlayer(),x,y);
        if (x > 3 || y > 3 || x < 1 || y < 1) {
            throw new IndexOutOfBoundsException(COORDINATE_ERR_MSG);
        } else {
            if (board[x-1][y-1].equals(UNOCCUPIED)){
                lastPlayer = getNextPlayer();
                board[x-1][y-1] = lastPlayer;
                return gameStatus(x-1, y-1);
            } else {
                throw new RuntimeException(FIELD_OCCUPIED_ERR_MSG);
            }
        }
    }

    private String gameStatus(int x, int y){
        if (getState().save(gameMove)){
            if (isDraw()){
                return RESULT_DRAW;
            }else if (isWin(x, y)){
                return String.format(RESULT_WINNER, lastPlayer);
            } else {
                return RESULT_IN_PROGRESS;
            }
        } else {
            throw new RuntimeException(SAVE_STATE_ERR_MSG);
        }
    }


    private boolean isWin(int x, int y) {
        HashSet<Character> horisontalSet = new HashSet();
        HashSet<Character> verticalSet = new HashSet();
        HashSet<Character> topDiagonalSet = new HashSet();
        HashSet<Character> bottomDiagonalSet = new HashSet();
        for (int i = 0; i < 3 ; i++) {
            if (board[x][i] == UNOCCUPIED) horisontalSet.add('f');
            if (board[i][y] == UNOCCUPIED) verticalSet.add('f');
            if (board[i][i] == UNOCCUPIED) topDiagonalSet.add('f');
            if (board[i][2-i] == UNOCCUPIED) bottomDiagonalSet.add('f');
            horisontalSet.add(board[x][i]);
            verticalSet.add(board[i][y]);
            topDiagonalSet.add(board[i][i]);
            bottomDiagonalSet.add(board[i][2-i]);
        }
        if (horisontalSet.size() == 1 || verticalSet.size() == 1 ||
                topDiagonalSet.size() == 1 || bottomDiagonalSet.size() == 1){
            return true;
        } else {
            return false;
        }
    }

    // Hint: if one cell is UNOCCUPIED false, else true
    private boolean isDraw() {
        for (int x = 0; x < SIZE; x++) {
            for (int y = 0; y < SIZE; y++) {
                if (board[x][y] == UNOCCUPIED) {
                    return false;
                }
            }
        }
        return true;
    }
}
