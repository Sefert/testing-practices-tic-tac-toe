package com.sybetech.business;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

//system does not smoke after invocation
@Ignore
public class TicTacToeSmokeTest {

    @Test
    public void whenDeployed_thenWebsiteIsReachable()  throws IOException {
        URL url = new URL("http://localhost:8080/ticTacToe");
        URLConnection  connection = url.openConnection();
        assertThat(connection.getContent(), notNullValue());
    }

    @Test
    public void whenDeployed_thenDatabaseIsReachable()  throws IOException {
        TicTacToeGameState state = new TicTacToeGameState();
        state.findById(4711);
    }
}
