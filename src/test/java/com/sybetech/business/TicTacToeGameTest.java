package com.sybetech.business;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Demo TDD. implement TicTacToeGame by first defining the spec here
 * Apply Red-Green-Refactore
 */
public class TicTacToeGameTest {

    // to simulate exception use Junit Rule ExpectedException
    // https://github.com/junit-team/junit4/wiki/rules
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private TicTacToeGame game;
    private TicTacToeGameState state;

    @Before
    public final void before() {
        // mock state
        state = mock(TicTacToeGameState.class);

        // mock clear
        doReturn(true).when(state).clear();

        // mock save for default saving
        doReturn(true).when(state).save(org.mockito.Mockito.any(TicTacToeGameMove.class));

        // init game
        game = new TicTacToeGame(state);
    }


    /****************************************************************************************
     * R1: play one piece within a board of 3x3
     ****************************************************************************************/

    /**
     * R1.1: throw RuntimeException if x not valid
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void whenXOutOfBoard_thenThrowException() {
         game.play(4,4);
    }

    /**
     * R1.2: throw RuntimeException if y not valid
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void whenYOutOfBoard_thenThrowException() {
        game.play(0,0);
    }

    /**
     * R1.3: throw RuntimeException if field on x,y occupied
     */
    @Test(expected = RuntimeException.class)
    public void whenOccupied_ThenThrowRuntimeException() {
        game.play(1,1);
        //exception.expect(RuntimeException.class);
        //exception.expectMessage("smth");
        game.play(1,1);
    }

    /*****************************************************************************************
     * R2: track last and next player
     ****************************************************************************************/

    /**
     * R2.1: first player is X
     */
    @Test
    public void firstPlayerIsX() {
        game.play(1,1);
        assertThat(game.getLastPlayer(), equalTo('X'));
    }

    /**
     * R2.2: if lastplayer was X, nextplayer is O
     */
    @Test
    public void whenSelectedPlayerWasX_thenNextIsO() {
        game.play(1,1);
        assertThat(game.getNextPlayer(), equalTo('O'));
    }

    /**
     * R2.3: if lastplayer was O, nextplayer is X
     * N.B: usefull?
     */
    @Test
    public void whenSelectedPlayerWasO_thenNextIsX() {
        game.play(1,1);
        game.play(2,2);
        assertThat(game.getNextPlayer(), equalTo('X'));
    }


    /*****************************************************************************************
     * R3: player who connects a line (horizontal, vertical, or diagonal) first wins. Else draw
     ****************************************************************************************/

    /**
     * R3.1: before line connected play is in progress
     */
    @Test
    public void whenPlay_ThenInProgress() {
        String result = game.play(1,1);
        assertThat(result, equalTo(TicTacToeGame.RESULT_IN_PROGRESS));
    }

    /**
     * R3.2: player who connects horizontal line first wins
     */
    @Test
    public void whenPlayAndHorizontalLineFilled_ThenWinner() {
        game.play(1,1);
        game.play(2,1);
        game.play(1,2);
        game.play(2,2);
        assertThat(game.play(1,3), equalTo(String.format(TicTacToeGame.RESULT_WINNER, 'X')));
    }

    /**
     * R3.3: player who connects vertical line first wins
     */
    @Test
    public void whenPlayAndVerticalLineFilled_ThenWinner() {
        game.play(1,1);
        game.play(1,2);
        game.play(2,1);
        game.play(2,2);
        assertThat(game.play(3,1), equalTo(String.format(TicTacToeGame.RESULT_WINNER, 'X')));
    }

    /**
     * R3.4: player who connects top-bottom diagonal line first wins
     */
    @Test
    public void whenPlayAndTopBottomDiagonalLineFilled_ThenWinner() {
        game.play(1,1);
        game.play(1,2);
        game.play(2,2);
        game.play(2,1);
        assertThat(game.play(3,3), equalTo(String.format(TicTacToeGame.RESULT_WINNER, 'X')));
    }

    /**
     * R3.5: player who connects bottom-top diagonal line first wins
     */
    @Test
    public void whenPlayAndBottomTopDiagonalLineFilled_ThenWinner() {
        game.play(3,1);
        game.play(1,2);
        game.play(2,2);
        game.play(2,1);
        assertThat(game.play(1,3), equalTo(String.format(TicTacToeGame.RESULT_WINNER, 'X')));
    }

    /**
     * R3.6: play is draw if all fields are set and no line connected
     */
    @Test
    public void whenAllFieldsFilled_ThenDraw() {
        game.play(1,1);
        game.play(1,2);
        game.play(1,3);
        game.play(2,2);
        game.play(2,1);
        game.play(3,1);
        game.play(2,3);
        game.play(3,3);
        assertThat(game.play(3,2), equalTo(TicTacToeGame.RESULT_DRAW));
    }


    /*****************************************************************************************
     * R4: save each move to DB and clean DB for each new game session
     ****************************************************************************************/

    /**
     * R4.1: game state should be initialized at session begin (intantiation)
     */
    @Test
    public void whenInstantiated_ThenSetState() {
        assertThat(game.getState(), notNullValue());
    }

    /**
     * R4.2: each game move should be saved to DB. Focus on current class not external dependencies
     */
    @Test
    public void whenPlay_ThenSaveMoveIsInvoked() {
        TicTacToeGameMove move = new TicTacToeGameMove(1,'X',1,1);
        game.play(move.getX(),move.getY());
        verify(state, times(1)).save(eq(move));
        move = new TicTacToeGameMove(2,'O',1,2);
        game.play(move.getX(),move.getY());
        verify(state, times(1)).save(move);
    }
    /**
     * R4.3: throw RuntimeException if save failed. Save should defaultly return true
     */
    @Test(expected = RuntimeException.class)
    public void whenPlayAndSaveReturnsFalse_ThenThrowRuntimeException() {
        //TicTacToeGameMove move = new TicTacToeGameMove(1,'X',1,1);
        doReturn(false).when(state).save(org.mockito.Mockito.any(TicTacToeGameMove.class));
        //when(game.getState().save(move)).thenThrow(RuntimeException.class);
        //exception.expect(RuntimeException.class);
        game.play(1,1);
    }

    /**
     * R4.4: the move should increase after each play
     */
    @Test
    public void whenPlayMultipleTimes_ThenTurnIncreased() {
        TicTacToeGameMove move = new TicTacToeGameMove(1,'X',1,1);
        //when(game.getState()).thenReturn(state);
        game.play(move.getX(),move.getY());
        move = new TicTacToeGameMove(2,'O',1,2);
        game.play(move.getX(),move.getY());
        assertThat(game.getMove(), equalTo(2));
    }

    /**
     * R4.5: game state data should be cleared at session begin (intantiation)
     */
    @Test
    public void whenInstantiated_ThenStateClearInvoked() {
        verify(state, times(1)).clear();
    }

    /**
     * R4.6: throw RuntimeException if clearing state data fails
     */
    @Test(expected = RuntimeException.class)
    public void whenInstantiatedAndClearReturnsFalse_ThenThrowRuntimeException() {
        doReturn(false).when(state).clear();
        game = new TicTacToeGame(state);
    }

    /*****************************************************************************************
     * R5: use MongoDB as persistent storage for the game state. (TicTacToeGameStateTest)
     ****************************************************************************************/


    /****************************************************************************************
     * BP1: check the code coverage
     ****************************************************************************************/

    /****************************************************************************************
     * BP2: integrate real MongoDb. implement TicTacToeIntTest
     ****************************************************************************************/

    /****************************************************************************************
     * R7: create a web UI to play
     * implement TicTacToeGameUiController
     ****************************************************************************************/

    /****************************************************************************************
     * R8: create automated systemtest using ui. implement TicTacToeGameUiTest (UI-Testing)
     ****************************************************************************************/

    /****************************************************************************************
     * R9: create automated acceptancetest using ui and cucumber.
     * implement TicTacToeCucumberSteps
     ****************************************************************************************/
}
