package com.sybetech.business;

import org.junit.*;
import org.junit.rules.TestName;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * JUnit Demo
 * use assertEquals, assertTrue (with and without message
 */
public class TicTacToeGameMoveTest {

    private TicTacToeGameMove move;
    private final int id = 1;
    private final int x = 2;
    private final int y = 3;
    private final char player = 'X';

    // https://github.com/junit-team/junit4/wiki/rules (JUnit interceptors)
    @Rule
    public final TestName name = new TestName();

    @BeforeClass
    public static void beforeClass() {
        // called once on class initialization time
        System.out.println("beforeClass");
    }

    @AfterClass
    public static void afterClass() {
        // called once after all methods are executed
        System.out.println("afterClass");
    }

    @Before
    public void setupMove() {
        // called before each method execution
        move = new TicTacToeGameMove(id,player,x,y);
        System.out.println("setupMove");
    }

    @After
    public void after() {
        // called after each method execution
        move = null;
        System.out.println("after");
    }

    @Test
    public void whenInstantiated_ThenIdIsSet() {
        assertEquals("check id is set after instantiation",1,move.getId());
        System.out.println(name.getMethodName());
    }

    @Test
    public void whenInstantiated_ThenXIsSet() {
        assertEquals("check x is set after instantiation",2,move.getX());
        System.out.println(name.getMethodName());
    }

    @Test
    public void whenInstantiated_ThenYIsSet() {
        assertEquals("check y is set after instantiation",3,move.getY());
        System.out.println(name.getMethodName());
    }

    @Test
    public void whenInstantiated_ThenPlayerIsSet() {
        assertEquals("check player is set after instantiation",'X', move.getPlayer());
        System.out.println(name.getMethodName());
    }

    @Test
    public void whenInstantiated_ThenAllowedCharsAreOandX() {
        assertTrue("check allowedChars are O and X after instantiation",move.getAllowedChars().
                containsAll(Arrays.asList('O','X')));
        assertEquals(2,move.getAllowedChars().size());
        System.out.println(name.getMethodName());
    }

    @Test
    public void whenGetMethodNameOfTestNameRuleInvoked_ThenReturnNameOfThisTest() {
        assertEquals("whenGetMethodNameOfTestNameRuleInvoked_ThenReturnNameOfThisTest", name.getMethodName());
    }

}