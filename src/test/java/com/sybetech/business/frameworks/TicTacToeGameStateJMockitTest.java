/*package com.sybetech.business.frameworks;

import com.mongodb.MongoException;
import com.sybetech.business.TicTacToeGameMove;
import com.sybetech.business.TicTacToeGameState;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.jongo.MongoCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

//import org.junit.jupiter.api.Test;
//https://stackoverflow.com/questions/24319697/java-lang-exception-no-runnable-methods-exception-in-running-junits


*//**
 * Demo for mocking framework JMockit
 *//*

@RunWith(JMockit.class)
public class TicTacToeGameStateJMockitTest {

    @Tested //instantiates state
    private TicTacToeGameState state;

    @Mocked
    private MongoCollection mongoCollection;

    private TicTacToeGameMove move;

    @Before
    public void setUp() {
        move = new TicTacToeGameMove(1, 'X', 2, 3);

        // instantiate class to test
        //tate = new TicTacToeGameState();
        // use Deencapsulation to set private field mongoCollection
        //Deencapsulation.setField(state,"mongoCollection",mongoCollection); for
    }

    // check save invocation.
    // use NonStrictExpectations and times to mock mongoCollection.save with old 1.9
    // new 1.46 https://jmockit.github.io/tutorial/Mocking.html
    @Test
    public void whenSave_ThenInvokeMongoCollectionSaveAndReturnTrue() throws Exception {
        new Expectations(){
            {
                mongoCollection.save(any);
                times = 1;
            }};
        boolean result = state.save(move);
        assertThat(result, equalTo(true));
    }


    // check when exception occurs during save, then save returns false
    // use Expectations and result to mock mongoCollection.save
    @Test
    public void givenMongoException_WhenSave_ThenReturnFalse() {
        new Expectations(){
            {
                mongoCollection.save(any);
                times = 1;
                result = new MongoException("Failed");
            }};
        boolean result = state.save(move);
        assertThat(result, equalTo(false));
    }

    // check clear invocation
    // use Expectations to mock mongoCollection.drop()
    @Test
    public void whenClear_ThenInvokeMongoCollectionDrop() {
        boolean result = state.clear();
        assertThat(result, equalTo(true));
        new Verifications(){
            {
                mongoCollection.drop();
                times = 1;
            }};
    }

    // check when exception occurs during clear, then clear returns false
    // use Expectations and result to mock mongoCollection.drop
    @Test
    public void givenMongoException_WhenClear_ThenReturnFalse() {
        new Expectations(){
            {
                mongoCollection.drop();
                times = 1;
                result = new MongoException("Failed drop");
            }};
        assertThat(state.clear(), equalTo(false));
    }
}*/
