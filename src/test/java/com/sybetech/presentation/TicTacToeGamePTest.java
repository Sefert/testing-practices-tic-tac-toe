package com.sybetech.presentation;

import com.sybetech.business.TicTacToeGame;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.Date;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Stress Test.
 * Works only after deployment
 */
//@Ignore
public class TicTacToeGamePTest {

	private WebDriver driver;
	//TicTacToeGameUiTest playARound;

	@Before
	public void setUp() {
		driver = new HtmlUnitDriver();
		//playARound = new TicTacToeGameUiTest();
	}

	@Test
	public void stressTest() throws Exception{
		System.out.println("Testing 1000 rounds Begin at: " + new Date());
		driver.get(Constants.APP_URL);
		assertThat(driver.getTitle(), equalTo(Constants.APP_TITLE));
		for (int i = 0; i < 1000; i++) {
			playAndCheckARound();
		}
		System.out.println("Testing 1000 rounds Begin at: " + new Date());
	}

	private void playAndCheckARound() throws Exception {
		assertThat(play(1,1), equalTo("X"));
		assertThat(play(1,2), equalTo("O"));
		assertThat(play(2,1), equalTo("X"));
		assertThat(play(2,3), equalTo("O"));
		assertThat(play(3,1), equalTo("X"));

		WebElement result = driver.findElement(By.name("f:result"));
		assertThat(result.getAttribute("value"), equalTo(String.format(TicTacToeGame.RESULT_WINNER, 'X')));

		WebElement btn = driver.findElement(By.name("f:reset"));
		btn.submit();
	}

	private String play(int x, int y) {
		WebElement btn = driver.findElement(By.name(String.format("f:btn%s_%s", x, y)));
		btn.submit();
		btn = driver.findElement(By.name(String.format("f:btn%s_%s", x, y)));
		return btn.getAttribute("value");
	}

}
