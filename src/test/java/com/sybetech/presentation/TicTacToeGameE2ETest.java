package com.sybetech.presentation;

import com.sybetech.business.TicTacToeGame;
import com.sybetech.business.TicTacToeGameMove;
import com.sybetech.business.TicTacToeGameState;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * System Test.
 * Works only after deployment
 */
//@Ignore
public class TicTacToeGameE2ETest {

	private WebDriver driver;

	@Before
	public void setUp() {
		driver = new HtmlUnitDriver();
	}

	@Test
	public void whenPlayAndWholeHorizontalLineFilled_ThenWinner() throws Exception {
		driver.get(Constants.APP_URL);
		assertThat(driver.getTitle(), equalTo(Constants.APP_TITLE));

		assertThat(play(1,1), equalTo("X"));
		TicTacToeGameState state = new TicTacToeGameState();
		TicTacToeGameMove persistedMove = state.findById(1);
		TicTacToeGameMove excpectedMove = new TicTacToeGameMove(1,'X',1,1);
		assertThat(excpectedMove, equalTo(persistedMove));

		assertThat(play(1,2), equalTo("O"));
		persistedMove = state.findById(2);
		excpectedMove = new TicTacToeGameMove(2,'O',1,2);
		assertThat(excpectedMove, equalTo(persistedMove));

		assertThat(play(2,1), equalTo("X"));
		persistedMove = state.findById(3);
		excpectedMove = new TicTacToeGameMove(3,'X',2,1);
		assertThat(excpectedMove, equalTo(persistedMove));

		assertThat(play(2,3), equalTo("O"));
		persistedMove = state.findById(4);
		excpectedMove = new TicTacToeGameMove(4,'O',2,3);
		assertThat(excpectedMove, equalTo(persistedMove));

		assertThat(play(3,1), equalTo("X"));
		persistedMove = state.findById(5);
		excpectedMove = new TicTacToeGameMove(5,'X',3,1);
		assertThat(excpectedMove, equalTo(persistedMove));

		WebElement result = driver.findElement(By.name("f:result"));
		assertThat(result.getAttribute("value"), equalTo(String.format(TicTacToeGame.RESULT_WINNER, 'X')));
	}

	private String play(int x, int y) {
		WebElement btn = driver.findElement(By.name(String.format("f:btn%s_%s", x, y)));
		btn.submit();
		btn = driver.findElement(By.name(String.format("f:btn%s_%s", x, y)));
		return btn.getAttribute("value");
	}

}
